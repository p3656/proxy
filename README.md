# Proxy



## Description

This app is a simple express server which is used as a proxy between the clients and web-app/api. It was made to solve the constraint on the production server that allows only the port 80 to be used. There is no point in using in for development purpose.

## Start

Install dependencies
$ yarn

Start the app
$ yarn start
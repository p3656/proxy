const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');

// Create Express Server
const app = express();

// Configuration
const PORT = 2000;
const API_URL = "http://162.38.114.223:5001";
const FRONT_URL = "http://162.38.114.223:3000";

app.use('/api/*', createProxyMiddleware({
    target: API_URL,
    changeOrigin: true,
    }
));

// Every url that does not start with /api
app.use(/^\/(?!api).*/, createProxyMiddleware({
    target: FRONT_URL,
    changeOrigin: true,
    }
));

// Start the Proxy
app.listen(PORT);
# syntax=docker/dockerfile:1

FROM node:17.4.0
ENV NODE_ENV=production

WORKDIR /app

COPY ["package.json", "yarn.lock*", "./"]

RUN yarn install --production

COPY . .

CMD [ "npm", "run", "start"]